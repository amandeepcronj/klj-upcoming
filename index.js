const express = require('express');
const path = require('path')

const app = express();

const server = app.listen(9000, () => {
  console.log(`Server listen to 9000`);
});

// To add upload video frontend using same domain by making it public.
app.use(express.static(path.resolve("./assets")));
app.use(express.static(path.resolve("./public")));
module.exports = server;
